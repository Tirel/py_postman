from PyQt6 import QtWidgets
from mywindow import Ui_MainWindow
import sys


class windows(QtWidgets.QMainWindow):  

    def __init__(self, ConstType):
        super(windows, self).__init__()
        self.ui = ConstType()
        self.ui.setupUi(self)
        if ConstType == Ui_MainWindow:
            pass

app = QtWidgets.QApplication([])
application = windows(Ui_MainWindow)

application.show()
sys.exit(app.exec())